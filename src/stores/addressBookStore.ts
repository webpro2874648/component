import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook {
  id: number
  name: string
  gender: string
  tel: string
}
export const useaddressBookStore = defineStore('address_book', () => {
  let lastId = 1

  const address = ref<AddressBook>({
    id: 0,
    name: '',
    gender: 'Male',
    tel: ''
  })
  const addressList = ref<AddressBook[]>([])
  const isAddnew = ref(false)
  function save() {
    if (address.value.id > 0) {
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    } else { //add new
      addressList.value.push({ ...address.value, id: lastId++ })
    }
    isAddnew.value = false
    address.value = {
      id: 0,
      name: '',
      gender: 'Male',
      tel: ''
    }
  }
  function edit(id: number) {
    isAddnew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex])) //copy object
  }
  function remove(id: number) {
    const removeIndex = addressList.value.findIndex((item) => item.id === id)
    addressList.value.splice(removeIndex) //copy object
  }
  function cancel() {
    isAddnew.value = false
    address.value = {
      id: 0,
      name: '',
      gender: 'Male',
      tel: ''
    }
  }

  return { address, addressList, save, isAddnew, edit, remove, cancel }
})
